package it.tecnest.spikes;

import java.util.*;
import java.util.logging.Logger;

/**
 * Hello world!
 */
public class App {
    private static Logger logger = Logger.getLogger("HOHOH");

    public static void main(String... args) {
        logger.info("Hello World!");
        String dontDoThis = "dontDoThis";
        boolean checkl = (dontDoThis == "dontDoThis");
        
        //
        // Intentionally wrong to test code analysis tool
        if (dontDoThis == "dontDoThis") {
            // empty block
        }
    }
}
