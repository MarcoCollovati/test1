#!groovy
pipeline {
    agent any
    stages {
        stage('Preparation') {
            steps {
                sh 'env | sort'
                sh './mvnw clean package'
	        }
        }
        stage('Only on PR') {
            when {
                changeRequest()
            }
            steps {
                sh 'echo "This is a PR"'
                configFileProvider([configFile(fileId: 'sonar-mvn-settings', variable: 'MVN_SETTINGS')]) {
                    sh './mvnw -s $MVN_SETTINGS -PsonarPR -DskipTests checkstyle:checkstyle spotbugs:spotbugs pmd:pmd -Dlocation=google_checks.xml pmd:pmd verify sonar:sonar --batch-mode --errors ' +
                            '     -Dspotbugs.effort=Max -Dspotbugs.threshold=Low ' +
                            '     -Dsonar.bitbucket.repoSlug=test1 ' +
                            '     -Dsonar.bitbucket.accountName=MarcoCollovati ' +
                            '     -Dsonar.bitbucket.branchName=$CHANGE_BRANCH -Dsonar.bitbucket.pullRequestId=$CHANGE_ID'
                }
            }
        }
        stage('Only on Develop') {
            when {
                branch 'development'
            }
            steps {
                sh 'echo "This runs on develop"'
                configFileProvider([configFile(fileId: 'sonar-mvn-settings', variable: 'MVN_SETTINGS')]) {
                    sh './mvnw -s $MVN_SETTINGS -DskipTests checkstyle:checkstyle spotbugs:spotbugs pmd:pmd -Dlocation=google_checks.xml pmd:pmd verify sonar:sonar --batch-mode --errors ' +
                            '     -Dspotbugs.effort=Max -Dspotbugs.threshold=Low '
                }
            }
        }
        stage('Only on Master') {
            when {
                branch 'master'
            }
            steps {
                sh 'echo "This runs on master"'
            }
        }
        stage('Only on Master or Develop') {
            when {
                anyOf {
                    branch 'master'
                    branch 'development'
                }
            }
            steps {
                sh 'echo "This runs on master and develop"'
            }
        }
        stage('Release') {
            when {
                branch 'master'
            }
            steps {
                input message: 'Release artifacts', submitterParameter: 'APPROVER', parameters:[
                        string(defaultValue: '1', description: 'The version for this artifact', name: 'ARTIFACT_VERSION', trim: true),
                        string(defaultValue: '', description: '', name: 'ARTIFACT_NEXT_VERSION', trim: true)
                    ]

                sh 'echo "Will release $ARTIFACT_VERSION and set new development version to $ARTIFACT_NEXT_VERSION"'
            }
        }
    }
}
